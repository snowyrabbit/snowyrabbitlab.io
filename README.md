# HP用ウェブサイトのレポジトリ
個人用ウェブサイト。ポートフォリオとか自己紹介。

## 使っている技術
- Hugo：静的サイトジェネレーターとして使用
- Gitlab Pages：ホスティングサービスとして使用

サイトのテンプレートにHugoテーマの[hermit](https://themes.gohugo.io/hermit/)を使用しています。
