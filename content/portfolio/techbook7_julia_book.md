---
title: "Julia言語の構文解説本"
date: 2019-09-22
draft: false
images: 
    - portfolio/juliabook_cover.jpg
tags:
  - Julia
---

---

## 使っている技術
- Julia(v1.2)
- Jupyter Notebook（執筆環境として）
- レポジトリ: https://gitlab.com/snowyrabbit/techbook7-julia-book

---


技術書典７で販売したJuliaの構文まとめ本です。
変数、配列、関数といったプログラミング言語おきまりの構文から、
Juliaの特徴的な機能である多重ディスパッチ、マクロについても解説しています。

販売した書籍は[Booth](https://booth.pm/ja/items/1571615)から購入可能です。