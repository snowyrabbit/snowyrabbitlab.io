---
title: "Julia製WebフレームワークGenieで開発した本棚Webアプリ"
date: 2019-04-21
draft: false
images: 
      - portfolio/cover.jpg
tags:
  - Julia
  - Genie
---

---

## 使っている技術
- フロントエンド: HTML, CSS, Bootstrap
- サーバーサイド言語: Julia(v1.1)
- フレームワーク: Genie(JuliaのMVCフレームワーク)
- DB: SQLite3
- レポジトリ: https://gitlab.com/snowyrabbit/geniebookshelf

---


{{< figure src="/images/geniebookshelf_home.jpg" title="アプリのホーム画面" class="center" width="700" height="700" >}}


技術書典６で販売した書籍のために開発したサンプルアプリです。


バックエンドはJuliaで動いています。


本の登録／編集／更新／削除といった簡単なCRUD機能を実装しました。

販売した書籍は[Booth](https://booth.pm/ja/items/1316756)から購入可能です。